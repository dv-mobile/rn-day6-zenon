import React, { Component } from 'react'
import { ScrollView, View, Image } from 'react-native'
import style from './style'
import { WhiteSpace, Flex, Icon, Button, InputItem, List } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import { saveUser } from 'src/redux/user/action'

class Login extends Component {

    state = {
        username: 'test2@gmail.com',
        password: '123456',
    }

    login = () => {
        const { username, password } = this.state
        if (!username) {
            alert('กรุณากรอก Username')
        }
        else if (!password) {
            alert('กรุณากรอก Password')
        }
        else {
            this.props.push('/product/list')
        }
    }

    render() {
        return (
            <ScrollView
                style={style.container}
                automaticallyAdjustContentInsets={false}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
            >
                <WhiteSpace />
                <View style={style.logoWrapper}>
                    <Image
                        source={require('src/assets/logo.png')}
                        style={style.logoImage}
                        resizeMode="contain"
                    />
                </View>
                <View style={style.formWrapper}>
                    <List>
                        <InputItem
                            placeholder="Username"
                            value={this.state.username}
                            onChange={username => this.setState({ username })}
                        >
                            <Flex justify="center" align="center">
                                <Icon name="user" />
                            </Flex>
                        </InputItem>
                        <InputItem
                            placeholder="Password"
                            type="password"
                            value={this.state.password}
                            onChange={password => this.setState({ password })}
                        >
                            <Flex justify="center" align="center">
                                <Icon name="key" />
                            </Flex>
                        </InputItem>
                    </List>
                </View>
                <Button
                    type="primary"
                    style={style.button}
                    onPress={this.login}
                >
                    Login
                </Button>
                <WhiteSpace />
            </ScrollView>
        )
    }
}

export default connect (
    null,
    { 
        saveUser,
        push
    }
)(Login)