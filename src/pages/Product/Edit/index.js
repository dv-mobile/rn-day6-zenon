import React, { Component, Fragment } from 'react'
import { ScrollView, View, Image } from 'react-native'
import style from './style'
import { WhiteSpace, Flex, Icon, Button, InputItem, List } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { goBack } from 'connected-react-router'
import { editProduct } from 'src/redux/products/action'
import Header from 'src/components/Header'

class ProductEdit extends Component {
    state = {
        imageUrl: '',
        name: ''
    }

    UNSAFE_componentWillMount() {
        const targetProduct = this.props.router.location.state
        this.product = this.props.products.find(product => product.id === targetProduct.id) 
        this.setState({
            imageUrl: this.product.image,
            name: this.product.name,
        })
    }

    save = () => {
        this.props.editProduct (
            this.product.id,
            {
                image: this.state.imageUrl,
                name: this.state.name
            }
        )
        this.props.goBack()
    }

    render() {
        const imageUrl = this.state.imageUrl || 'http://vollrath.com/ClientCss/images/VollrathImages/No_Image_Available.jpg'
        return (
            <ScrollView
                style={style.container}
                automaticallyAdjustContentInsets={false}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
            >
                <Header title="Product Edit" />
                <Image
                    source={{ uri: imageUrl }}
                    resizeMode="cover"
                    style={style.logoImage}
                />
                <WhiteSpace size="lg" />
                <View style={{flex: 1}}>
                    <List>
                        <InputItem
                            placeholder="Image URL"
                            value={this.state.imageUrl}
                            onChange={imageUrl => this.setState({ imageUrl })}
                        >
                            <Flex justify="center" align="center">
                                <Icon name="file-image" />
                            </Flex>
                        </InputItem>
                        <InputItem
                            placeholder="Name"
                            type="name"
                            value={this.state.name}
                            onChange={name => this.setState({ name })}
                        >
                            <Flex justify="center" align="center">
                                <Icon name="behance-square" />
                            </Flex>
                        </InputItem>
                    </List>
                    <Button
                        type="primary"
                        style={style.button}
                        onPress={this.save}
                    >
                        Save
                    </Button>
                    <WhiteSpace />
                </View>
            </ScrollView>
        )
    }
}

export default connect (
    ({ products, router }) => ({ products, router }),
    {
        editProduct,
        goBack
    }
)(ProductEdit)