import React, { Component, Fragment } from 'react'
import { ScrollView, View, Image } from 'react-native'
import style from './style'
import { WhiteSpace, Flex, Icon, Button, InputItem, List } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { createProduct } from 'src/redux/products/action'
import Header from 'src/components/Header'
import { goBack } from 'connected-react-router'

class ProductAdd extends Component {
    state = {
        imageUrl: '',
        name: ''
    }

    save = () => {
        const { name, imageUrl } = this.state
        this.props.createProduct(name, imageUrl)
        this.props.goBack()
    }

    render() {
        const imageUrl = this.state.imageUrl || 'http://vollrath.com/ClientCss/images/VollrathImages/No_Image_Available.jpg'
        return (
            <ScrollView
                style={style.container}
                automaticallyAdajustContentInsets={false}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
            >
                <Header title="Product Add" />
                <Image
                    source={{ uri: imageUrl }}
                    resizeMode="cover"
                    style={style.logoImage}
                />
                <WhiteSpace size="lg" />
                <View style={{flex: 1}}>
                    <List>
                        <InputItem
                            placeholder="Image URL"
                            value={this.state.imageUrl}
                            onChange={imageUrl => this.setState({ imageUrl })}
                        >
                            <Flex justify="center" align="center">
                                <Icon name="file-image" />
                            </Flex>
                        </InputItem>
                        <InputItem
                            placeholder="Name"
                            type="name"
                            value={this.state.name}
                            onChange={name => this.setState({ name })}
                        >
                            <Flex justify="center" align="center">
                                <Icon name="behance-square" />
                            </Flex>
                        </InputItem>
                    </List>
                    <Button
                        type="primary"
                        style={style.button}
                        onPress={this.save}
                    >
                        Save
                    </Button>
                    <WhiteSpace />
                </View>
            </ScrollView>
        )
    }
}

export default connect (
    null,
    {
        createProduct,
        goBack
    }
)(ProductAdd)