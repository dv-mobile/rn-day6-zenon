import React, { Component } from 'react'
import { ScrollView, View, Image } from 'react-native'
import style from './style'
import { WhiteSpace, Button } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import Header from 'src/components/Header'

class ProductDetail extends Component {

    UNSAFE_componentWillMount() {
        const targetProduct = this.props.router.location.state
        this.product = this.props.products.find(product => product.id === targetProduct.id)
    }

    edit = () => {
        this.props.push('/product/edit', this.product)
    }

    render() {
        return (
            <ScrollView
                style={style.container}
                automaticallyAdjustContentInsets={false}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
            >
                <Header title={this.product.name} />
                <Image
                    source={{ uri: this.product.image }}
                    resizeMode="cover"
                    style={style.logoImage}
                />
                <WhiteSpace size="lg" />
                <View style={{flex: 1}}>
                    <Button
                        type="primary"
                        style={style.button}
                        onPress={this.edit}
                    >
                        Edit
                    </Button>
                    <WhiteSpace />
                </View>
            </ScrollView>
        )
    }
}

export default connect(
    ({ products, router }) => ({ products, router }),
    {
        push
    }
)(ProductDetail)