import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEE'
    },
    logoWrapper: { 
        margin: 40,
    },
    logoImage: {
        width: '100%',
        height: 200,
    },
    button: {
        marginTop: 80,
        marginHorizontal: 16,
    }
})