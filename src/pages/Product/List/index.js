import React, { Component, Fragment } from 'react'
import { Text, ScrollView, View, Image, TouchableOpacity } from 'react-native'
import style from './style'
import { Card, Grid, WhiteSpace, Flex, Icon, Button, InputItem, List } from '@ant-design/react-native'
import TabBottom from './TabBottom'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'

class ProductList extends Component {
    goToProductDetail = (product) => {
        this.props.push('/product/detail', product)
    }
    renderItem = (item) => {
        return (
            <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => this.goToProductDetail(item)}
            >
                <Card style={style.cardWrapper}>
                    <Image
                        source={{uri: item.image}}
                        resizeMode="cover"
                        style={style.cardImage}
                    />
                    <View style={style.cardTextWrapper}>
                        <Text style={style.cardText}>{item.name}</Text>
                    </View>
                </Card>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <Fragment>
                <ScrollView
                    style={style.container}
                    automaticallyAdjustContentInsets={false}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 100 }}
                >
                    <WhiteSpace />
                    <View style={style.logoWrapper}>
                        <Image
                            source={require('src/assets/logo.png')}
                            style={style.logoImage}
                            resizeMode="contain"
                        />
                    </View>
                    <WhiteSpace />
                    <View>
                        <Grid
                            data={this.props.products}
                            columnNum={2}
                            renderItem={this.renderItem}
                        />
                    </View>
                    
                </ScrollView>
                <TabBottom />
            </Fragment>
        )
    }
}

export default connect(
    ({ products, user }) => ({ products, user }),
    {
        push
    }
)(ProductList)