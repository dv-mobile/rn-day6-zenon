import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Icon } from '@ant-design/react-native'
import style from './style'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'

class TabBottom extends Component {
    render() {
        return (
            <View style={style.bottomContainer}>
                <TouchableOpacity
                    style={style.bottomMenu}
                    onPress={() => this.props.push('/product/add')}
                >
                    <Icon name="folder-add" />
                    <Text style={style.bottomMenuText}>Add Product</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={style.bottomMenu}
                    onPress={() => this.props.push('/user/edit')}
                >
                    <Icon name="user" />
                    <Text style={style.bottomMenuText}>Edit User</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default connect(
    null,
    {
        push
    }
)(TabBottom)