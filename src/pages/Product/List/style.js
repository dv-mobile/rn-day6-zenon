import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEE',
    },
    logoWrapper: { 
        margin: 20,
    },
    logoImage: {
        width: '100%',
        height: 100,
    },
    cardWrapper: {
        flex: 1,
        // padding: 20,
        // paddingTop: 10,
    },
    cardImage: {
        flex: 1,
    },
    cardTextWrapper: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        paddingVertical: 4,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardText: {
        color: '#FFF',
        fontWeight: 'bold'
    },
    bottomContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#FFF',
        flexDirection: 'row',
    },
    bottomMenu: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 12,
    },
    bottomMenuText: {
        marginTop: 2,
    }
})