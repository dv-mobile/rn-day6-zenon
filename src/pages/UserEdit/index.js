import React, { Component } from 'react'
import { ScrollView, View } from 'react-native'
import style from './style'
import { WhiteSpace, Flex, Icon, Button, InputItem, List } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { saveUser } from 'src/redux/user/action'
import { goBack } from 'connected-react-router'
import Header from 'src/components/Header'

class UserEdit extends Component {
    state = {
        firstname: '',
        lastname: '',
    }

    UNSAFE_componentWillMount() {
        this.setState(this.props.user)
    }

    save = () => {
        this.props.saveUser({
            ...this.state
        })
        this.props.goBack()
    }

    render() {
        return (
            <ScrollView
                style={style.container}
                automaticallyAdajustContentInsets={false}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
            >
                <Header title="User Edit" />
                <View style={{flex: 1}}>
                    <List>
                        <InputItem
                            placeholder="First Name"
                            value={this.state.firstname}
                            onChange={firstname => this.setState({ firstname })}
                        >
                            <Flex justify="center" align="center">
                                <Icon name="user" />
                            </Flex>
                        </InputItem>
                        <InputItem
                            placeholder="Last Name"
                            value={this.state.lastname}
                            onChange={lastname => this.setState({ lastname })}
                        >
                            <Flex justify="center" align="center">
                                <Icon name="behance-square" />
                            </Flex>
                        </InputItem>
                    </List>
                    <Button
                        type="primary"
                        style={style.button}
                        onPress={this.save}
                    >
                        Save
                    </Button>
                    <WhiteSpace />
                </View>
            </ScrollView>
        )
    }
}

export default connect(
    ({ user }) => ({ user }),
    {
        saveUser,
        goBack
    }
)(UserEdit)