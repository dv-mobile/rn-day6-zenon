import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEE'
    },
    button: {
        marginTop: 80,
        marginHorizontal: 16,
    }
})