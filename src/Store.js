import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import reducer from './redux'

export default (history) => {
    return createStore(
        reducer(history),
        undefined,
        compose (
            applyMiddleware (
                routerMiddleware(history)
            )
        )
    )
}