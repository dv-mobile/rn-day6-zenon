import React, { Component } from 'react'
import { View, Text } from 'react-native';
import { Route, Switch, Redirect } from 'react-router-native'
import { ConnectedRouter } from 'connected-react-router'

import Login from './pages/Login'
import ProductList from './pages/Product/List'
import ProductAdd from './pages/Product/Add'
import ProductDetail from './pages/Product/Detail'
import ProductEdit from './pages/Product/Edit'
import UserEdit from './pages/UserEdit'

export default class Router extends Component {
    render() {
        return (
            <ConnectedRouter history={this.props.history}>
                <Switch>
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/product/list" component={ProductList} />
                    <Route exact path="/product/add" component={ProductAdd} />
                    <Route exact path="/product/detail" component={ProductDetail} />
                    <Route exact path="/product/edit" component={ProductEdit} />
                    <Route exact path="/user/edit" component={UserEdit} />
                    <Redirect to="/login" />
                </Switch>
            </ConnectedRouter>
        )
    }
}