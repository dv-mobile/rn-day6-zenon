import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        height: 56,
        backgroundColor: '#FFF',
        elevation: 4,
    },
    titleWrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        textAlign: 'center',
        fontSize: 20,
    },
    closeButtonWrapper: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    closeButton: {
        padding: 20
    }
})