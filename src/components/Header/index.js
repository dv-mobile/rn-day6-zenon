import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Icon } from '@ant-design/react-native'
import style from './style'
import { connect } from 'react-redux'
import { goBack } from 'connected-react-router'

class Header extends Component {
    state = {  }
    render() {
        return (
            <View style={style.container}>
                <View style={style.titleWrapper}>
                    <Text style={style.title}>
                        {this.props.title}
                    </Text>
                </View>
                <View style={style.closeButtonWrapper}>
                    <TouchableOpacity
                        style={style.closeButton}
                        onPress={this.props.goBack}
                    >
                        <Icon name="arrow-left" />
                    </TouchableOpacity> 
                </View>
            </View>
        )
    }
}

export default connect (
    null,
    {
        goBack
    }
)(Header)