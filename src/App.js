import React, { Component } from 'react'
import { Provider } from 'react-redux'
import Router from './Router'

import { createMemoryHistory } from 'history'
import createStore from './Store'

import { createProduct } from 'src/redux/products/action'

export default class App extends Component {
    history = createMemoryHistory()
    store = createStore(this.history)
    UNSAFE_componentWillMount() {
        // Initial Product
        const initialProducts = [
            {
                image: 'https://th-test-11.slatic.net/p/b6e5d2bed09a8cb311116b9596f8bd2c.jpg_720x720q80.jpg_.webp',
                name: 'Core i5',
            },
            {
                image: 'https://th-test-11.slatic.net/original/b1733a693382f110c4cfe76004f01549.jpg_720x720q80.jpg_.webp',
                name: 'นาฬิกาข้อมือผู้ชาย',
            },
            {
                image: 'https://th-test-11.slatic.net/p/3/yifun-0862-70783718-ebc8c382b13dd0c0c2d7031f84a63e1a-catalog.jpg_720x720q80.jpg_.webp',
                name: 'Yifun ราวแขวนเสื้อ',
            },
            {
                image: 'https://th-test-11.slatic.net/original/72dee076d0c497062252a74bc61ad085.jpg_720x720q80.jpg_.webp',
                name: 'HONEI V BSC WHITE',
            },
            {
                image: 'https://th-test-11.slatic.net/p/50edb8a7534da782d2ef9a01af7c861c.jpg_720x720q80.jpg_.webp',
                name: 'ขายยกลัง Huggies Dry Pant',
            },
        ]
        initialProducts.forEach(product => {
            this.store.dispatch(createProduct(product.name, product.image))
        })
    }
    render() {
        return (
            <Provider store={this.store}>
                <Router history={this.history} />
            </Provider>
        )
    }
}