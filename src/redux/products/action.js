export const ADD_PRODUCT = 'ADD_PRODUCT'
export const EDIT_PRODUCT = 'EDIT_PRODUCT'

export const createProduct = (name, image) => ({
    type: ADD_PRODUCT,
    name,
    image,
})

export const editProduct = (targetId, payload) => ({
    type: EDIT_PRODUCT,
    targetId,
    payload,
})