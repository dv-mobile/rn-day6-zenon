import { ADD_PRODUCT, EDIT_PRODUCT } from './action'
import uuid from 'react-native-uuid'

const productReducer = (state = {}, action) => {
    switch (action.type) {
        case ADD_PRODUCT :
            return {
                id: uuid.v4(),
                name: action.name,
                image: action.image,
            }
        case EDIT_PRODUCT :
            return {
                ...state,
                ...action.payload
            }
        default :
            return state
    }
}

export default (state = [], action) => {
    switch (action.type) {
        case ADD_PRODUCT:
            return [...state, productReducer(null, action)]
        case EDIT_PRODUCT:
            return state.map(
                (product) => 
                    action.targetId === product.id ? 
                    productReducer(product, action) 
                    : 
                    product
                )
        default :
            return state
    }
}