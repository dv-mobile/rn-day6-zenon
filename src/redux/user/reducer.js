import { SAVE_USER } from './action'

export default (state = {}, action) => {
    switch (action.type) {
        case SAVE_USER :
            return {
                ...action.payload
            }
        default :
            return state
    }
}