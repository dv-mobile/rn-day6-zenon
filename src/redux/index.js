import { combineReducers } from 'redux'

import products from './products/reducer'
import user from './user/reducer'
import { connectRouter } from 'connected-react-router'

export default (history) => combineReducers({
    products,
    user,
    router: connectRouter(history)
})